<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SysIn
 */

get_header(); ?>

<?php get_sidebar(); ?>

    <div class="top_nav">
        <div class="nav_menu">
            <nav>
                <div class="nav toggle">
                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>

                <ul class="nav navbar-nav navbar-right">
                    <li class="">
                        <a class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	                        <?php
	                            date_default_timezone_set('America/Sao_Paulo');
	                        ?>
                            <i class="fa fa-clock-o"></i>
                            <span class="unit" id="date"><?php echo date('d/m/Y'); ?> - </span>
                            <span class="unit" id="time"><?php echo date('H:i'); ?></span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <div class="right_col" role="main">

        <?php
        if ( current_user_can('administrator') ) {
            include get_template_directory() . '/inc/panel.php';
        }
        ?>

        <div class="row">
            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="main-panel" class="x_panel">

                    <div class="x_title">
                        <h2><i class="fa fa-users"></i> Clientes
                            <small>(Lista completa)</small>
                        </h2>
                        <a id="register-client-btn" class="btn btn-primary" data-toggle="modal" data-target="#register-client-modal"><i class="fa fa-user-plus"></i> Cadastrar novo cliente</a>
                        <div class="clearfix"></div>
                    </div>

                    <div id="register-client-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

								<?php echo sysin_client_form(); ?>

                            </div>
                        </div>
                    </div>

                    <div id="delete-client-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Excluir cliente</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Tem certeza que deseje excluir o cliente: <b class="client-data"></b>?</p>
                                    <div class="alert alert-warning" role="alert">
                                        Atenção, isso <b>não</b> poderá ser desfeito!
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-danger" id="delete-client" data-delete-client="">Excluir cliente</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="client-checkin-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Cadastrar check-in</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="row">

                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <div class="x_content client-widget">
                                                <div class="flex">
                                                    <ul class="list-inline widget_profile_box">
                                                        <li>
                                                            <!--<a>
                                                                <i class="fa fa-pencil"></i>
                                                            </a>-->
                                                        </li>
                                                        <li class="client-avatar">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img.jpg" alt="..." class="img-circle profile_img">
                                                            <i class="hidden fa fa-photo"></i>
                                                        </li>
                                                        <li>
                                                            <!--<a>
                                                                <i class="fa fa-camera"></i>
                                                            </a>-->
                                                        </li>
                                                    </ul>
                                                </div>

                                                <h3 class="name"></h3>

                                                <div class="flex">
                                                    <ul>
                                                        <li>
                                                            <h4 class="cpf"></h4>
                                                            <span>CPF</span>
                                                        </li>
                                                        <li>
                                                            <h4 class="birthday"></h4>
                                                            <span>Data de nascimento</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="input-group date" id="client-checkin-wrapper">
                                                <input type="text" class="form-control" id="client-checkin" name="client-checkin">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>

                                            <a href="#" class="btn btn-primary btn-xs" id="do-client-checkin">
                                                <i class="fa fa-plus"></i> Cadastrar check-in
                                            </a>

                                            <div class="last-checkins">
                                                <h3>Check-ins
                                                    <small>Realizados</small>
                                                </h3>
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Dia</th>
                                                        <th>Hora</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="x_content">

                        <div id="clients-datatable-main-wrapper">
	                        <?php include get_template_directory() . '/inc/clients-table.php'; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();