<div id="sidebar" class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo home_url(); ?>" class="site_title">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-calaf.png" style="max-width: 50px;"> <span>CALAF</span>
            </a>
        </div>
        <div class="clearfix"></div>
        <div class="profile clearfix">
	        <?php $current_user = wp_get_current_user(); ?>
            <div class="profile_pic">
	            <?php echo get_avatar( $current_user->ID, 56, '', '', array( 'class' => array( 'img-circle', 'profile_img' ) ) ); ?>
            </div>
            <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?php echo $current_user->user_nicename; ?></h2>
            </div>
        </div>

        <br/>

        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Clientes</h3>
                <ul class="nav side-menu">
                    <li><a href="<?php echo home_url(); ?>"><i class="fa fa-users"></i>Listar clientes</a></li>
                    <?php if( is_home() ){ ?>
                    <li><a data-toggle="modal" data-target="#register-client-modal"><i class="fa fa-plus"></i> Cadastrar novo</a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo home_url('?modal=register-client-modal'); ?>"><i class="fa fa-plus"></i> Cadastrar novo</a></li>
                    <?php } ?>
                </ul>
            </div>
            <?php if ( current_user_can('administrator') ) : ?>
            <div class="menu_section">
                <h3>Usuários</h3>
                <ul class="nav side-menu">
                    <li><a href="<?php echo home_url('/usuarios'); ?>"><i class="fa fa-gears"></i>Listar usuários</a></li>
                    <?php if( is_page('usuarios') ){ ?>
                        <li><a data-toggle="modal" data-target="#users-modal"><i class="fa fa-plus"></i> Cadastrar novo</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo home_url('/usuarios?modal=users-modal'); ?>"><i class="fa fa-plus"></i> Cadastrar novo</a></li>
                    <?php } ?>
                </ul>
            </div>
            <?php endif; ?>

            <div class="menu_section">
                <h3>Sistema</h3>
                <ul class="nav side-menu">
                    <!--<li><a><i class="fa fa-smile-o"></i>Perfil</a></li>-->
                    <li><a href="<?php echo wp_logout_url(); ?>"><i class="fa fa-sign-out"></i>Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>