<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SysIn
 */

get_header(); ?>

<?php get_sidebar(); ?>

    <div class="top_nav">
        <div class="nav_menu">
            <nav>
                <div class="nav toggle">
                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>

                <ul class="nav navbar-nav navbar-right">
                    <li class="">
                        <a class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	                        <?php
	                            date_default_timezone_set('America/Sao_Paulo');
	                        ?>
                            <i class="fa fa-clock-o"></i>
                            <span class="unit" id="date"><?php echo date('d/m/Y'); ?> - </span>
                            <span class="unit" id="time"><?php echo date('H:i'); ?></span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <div class="right_col" role="main">

        <div class="row">
            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_title">
                        <h2><i class="fa fa-gears"></i> Usuários
                            <small>(Lista completa)</small>
                        </h2>
                        <a id="register-user-btn" class="btn btn-primary" data-toggle="modal" data-target="#users-modal"><i class="fa fa-user-plus"></i> Cadastrar novo usuário</a>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

	                    <?php
	                    if ( $_POST['sysin-submit'] ) {

	                        $editing = empty( $_POST['sysin-user-id'] ) ? false : true;
		                    $user_name = $_POST['sysin-login'];
		                    $name = $_POST['sysin-name'];
		                    $surname = $_POST['sysin-surname'];
		                    $email = $_POST['sysin-email'];
		                    $cpf = $_POST['sysin-cpf'];
		                    $gender = $_POST['sysin-gender'];
		                    $birthday = $_POST['sysin-birthday'];
		                    $role = $_POST['sysin-permissions'];
                            $photo = $_FILES['sysin-photo'];
		                    $error = false;
		                    $error_message = [];


	                        if ( username_exists( $user_name ) && !$editing ) {
		                        $error = true;
		                        array_push( $error_message, 'Este nome de usuário já está cadastrado' );
                            }

		                    if ( empty( $email ) ) {
			                    $error = true;
			                    array_push( $error_message, 'O campo email é de preenchimento obrigatório' );
		                    }

		                    if ( email_exists( $email ) && !$editing ) {
			                    $error = true;
			                    array_push( $error_message, 'Este email já está cadastrado' );
		                    }

		                    if (strlen( str_replace('.', '',  str_replace('-', '', $cpf) ) ) !== 11) {
			                    $error = true;
			                    array_push( $error_message, 'CPF Inválido' );
		                    }

		                    if( !$editing ) {
			                    $cpf_used = get_users( array( 'meta_key' => '_user_cpf', 'meta_value' => str_replace('.', '',  str_replace('-', '', $cpf) )  ) );
			                    if( $cpf_used ) {
				                    $error = true;
				                    array_push( $error_message, 'Este número de CPF já está sendo utilizado' );
			                    }
                            }

		                    if( !$error ) {
		                        if( !$editing ) : // New user
			                        $random_password = wp_generate_password( 8, false );
			                        $userdata = array(
				                        'user_login' => esc_attr($user_name),
				                        'first_name' => esc_attr($name),
				                        'last_name' => esc_attr($surname),
				                        'display_name' => esc_attr($name),
				                        'user_email' => esc_attr($email),
				                        'user_pass' => $random_password,
				                        'role' => $role
			                        );

			                        $user_id = wp_insert_user( $userdata ) ;
			                        if ( ! is_wp_error( $user_id ) ) {
				                        add_user_meta( $user_id, '_user_cpf', esc_attr($cpf), true );
				                        add_user_meta( $user_id, '_user_gender', esc_attr($gender), true );
				                        add_user_meta( $user_id, '_user_birthday', esc_attr($birthday), true );

                                        client_photo( $photo, $user_id );

				                        echo '<div class="alert alert-success text-center"><strong>Usuário cadastrado com sucesso.</strong></div>';
				                        unset( $_POST );
			                        } else {
				                        $error = true;
				                        array_push( $error_message, $user_id->get_error_code() );
			                        }
                                else : // Editing user
	                                $user_id = $_POST['sysin-user-id'];
	                                $userdata = array(
		                                'ID' => $user_id,
		                                'user_login' => esc_attr($user_name),
		                                'first_name' => esc_attr($name),
		                                'last_name' => esc_attr($surname),
		                                'display_name' => esc_attr($name),
		                                'user_email' => esc_attr($email),
		                                'role' => $role
	                                );

	                                if( wp_insert_user( $userdata ) ){
		                                update_user_meta( $user_id, '_user_cpf', esc_attr($cpf), true );
		                                update_user_meta( $user_id, '_user_gender', esc_attr($gender), true );
		                                update_user_meta( $user_id, '_user_birthday', esc_attr($birthday), true );

                                        client_photo( $photo, $user_id );

		                                echo '<div class="alert alert-success text-center"><strong>Usuário atualizado com sucesso.</strong></div>';
		                                unset( $_POST );
                                    } else {
		                                $error = true;
		                                array_push( $error_message, $user_id->get_error_code() );
	                                }
                                endif;

                            } else {
                                $error = true; ?>
                                <script>
                                    (function ($) {
                                        $(document).ready(function () {
                                            $('#users-modal').modal('show');
                                        });
                                    })(jQuery);
                                </script>
                            <?php }

	                    }

                        if ( !empty( $_POST['sysin-user-id'] ) ) {
	                        require_once(ABSPATH.'wp-admin/includes/user.php' );
	                        if( wp_delete_user( $_POST['sysin-user-id'] ) ){
		                        echo '<div class="alert alert-success text-center"><strong>Usuário deletado com sucesso.</strong></div>';
	                        } else {
		                        echo '<div class="alert alert-danger text-center"><strong>Houve um erro ao deletar o usuário.</strong></div>';
	                        }
                        }
	                    ?>

                        <div id="users-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Cadastro de usuário</h4>
                                    </div>
                                    <div class="modal-body">

                                        <form id="sysin-register-user-form"
                                              method="post"
                                              action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>"
                                              class="form-horizontal form-label-left"
                                              enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <div class="row">

                                                    <?php if( $error ): ?>
                                                    <div class="alert alert-danger text-center">
                                                        <?php
                                                        foreach ($error_message as $error){
                                                            echo '<strong>'. $error . '</strong><br>';
                                                        } ?>
                                                    </div>
	                                                <?php endif; ?>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-name">Login (nome de usuário) <b>*</b></label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input name="sysin-login" type="text" class="form-control login-field"
                                                                   value="<?php echo(isset($_POST['sysin-login']) ? $_POST['sysin-login'] : null); ?>"
                                                                   placeholder="" id="sysin-login" required/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-name">Nome <b>*</b></label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input name="sysin-name" type="text" class="form-control login-field"
                                                                   value="<?php echo(isset($_POST['sysin-name']) ? $_POST['sysin-name'] : null); ?>"
                                                                   placeholder="" id="sysin-name" required/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-surname">Sobrenome <b>*</b></label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input name="sysin-surname" type="text" class="form-control login-field"
                                                                   value="<?php echo(isset($_POST['sysin-surname']) ? $_POST['sysin-surname'] : null); ?>"
                                                                   placeholder="" id="sysin-surname" required/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-email">Email</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input name="sysin-email" type="email" class="form-control login-field"
                                                                   value="<?php echo(isset($_POST['sysin-email']) ? $_POST['sysin-email'] : null); ?>"
                                                                   placeholder="" id="sysin-email"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-cpf">CPF <b>*</b></label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input name="sysin-cpf" type="text" class="cpf form-control login-field"
                                                                   value="<?php echo (isset($_POST['sysin-cpf']) ? $_POST['sysin-cpf'] : null); ?>"
                                                                   placeholder="000.000.000-00" id="sysin-cpf" required/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-gender">Sexo</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div id="sysin-gender" class="btn-group" data-toggle="buttons">
                                                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                                    <input type="radio" name="sysin-gender" value="male"> &nbsp; Homem &nbsp;
                                                                </label>
                                                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                                    <input type="radio" name="sysin-gender" value="female"> Mulher
                                                                </label>
                                                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                                    <input type="radio" name="sysin-gender" value="undefined"> Indefinido
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-birthday">Data de nascimento</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input name="sysin-birthday" type="text" class="form-control login-field"
                                                                   value="<?php echo(isset($_POST['sysin-birthday']) ? $_POST['sysin-birthday'] : null); ?>"
                                                                   placeholder="DD/MM/AAAA" id="sysin-birthday"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-permissions">Permissão</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <select id="sysin-permissions" class="form-control" name="sysin-permissions">
                                                                <option value="editor">Editor</option>
                                                                <option value="administrator">Administrador</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-photo">Foto <b>*</b></label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input name="sysin-photo" type="file" class="form-control-file login-field"
                                                                   value="<?php echo(isset($_POST['sysin-photo']) ? $_POST['sysin-photo'] : null); ?>"
                                                                   placeholder="" id="sysin-photo" capture="camera"/>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-primary" type="button" data-dismiss="modal">Cancelar</button>
                                                <input type="hidden" name="sysin-user-id" id="sysin-user-id" value="<?php echo(isset($_POST['sysin-user-id']) ? $_POST['sysin-user-id'] : null); ?>">
                                                <input type="submit" name="sysin-submit" id="sysin-submit" class="btn btn-success" value="Cadastrar usuário">
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="delete-user-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form id="sysin-delete-user-form" method="post" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title">Excluir usuário</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Tem certeza que deseje excluir o cliente: <b class="client-data"></b>?</p>
                                            <div class="alert alert-warning" role="alert">
                                                Atenção, isso <b>não</b> poderá ser desfeito!
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            <input type="hidden" name="sysin-user-id" id="sysin-user-id" value="">
                                            <button type="submit" class="btn btn-danger" id="delete-client" data-delete-client="">Excluir cliente</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div id="clients-datatable-main-wrapper">
	                        <?php include get_template_directory() . '/inc/users-table.php'; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();