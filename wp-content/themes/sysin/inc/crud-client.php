<?php
function sysin_client_form($client_id = false){

	if ( $_POST['sysin-submit'] ) {
		validation();
		registration();
	}

	ob_start(); ?>

	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo ( $client_id ) ? 'Editar cliente' : 'Cadastro cliente';?></h4>
    </div>
	<form id="sysin-register-form" method="post" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>" class="form-horizontal form-label-left">
    <div class="modal-body">
			<div class="row">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-name">Nome <b>*</b></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input name="sysin-name" type="text" class="form-control login-field"
						       value="<?php echo(isset($_POST['sysin-name']) ? $_POST['sysin-name'] : null); ?>"
						       placeholder="" id="sysin-name" required/>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-surname">Sobrenome <b>*</b></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input name="sysin-surname" type="text" class="form-control login-field"
						       value="<?php echo(isset($_POST['sysin-surname']) ? $_POST['sysin-surname'] : null); ?>"
						       placeholder="" id="sysin-surname" required/>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-email">Email</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input name="sysin-email" type="email" class="form-control login-field"
						       value="<?php echo(isset($_POST['sysin-email']) ? $_POST['sysin-email'] : null); ?>"
						       placeholder="" id="sysin-email"/>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-cpf">CPF <b>*</b></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input name="sysin-cpf" type="text" class="cpf form-control login-field"
						       value="<?php echo (isset($_POST['sysin-cpf']) ? $_POST['sysin-cpf'] : null); ?>"
						       placeholder="000.000.000-00" id="sysin-cpf" required/>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-gender">Sexo</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="sysin-gender" class="btn-group" data-toggle="buttons">
							<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
								<input type="radio" name="sysin-gender" value="male"> &nbsp; Homem &nbsp;
							</label>
							<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
								<input type="radio" name="sysin-gender" value="female"> Mulher
							</label>
							<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
								<input type="radio" name="sysin-gender" value="undefined"> Indefinido
							</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-birthday">Data de nascimento</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input name="sysin-birthday" type="text" class="form-control login-field"
						       value="<?php echo(isset($_POST['sysin-birthday']) ? $_POST['sysin-birthday'] : null); ?>"
						       placeholder="DD/MM/AAAA" id="sysin-birthday"/>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-photo">Foto <b>*</b></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input name="sysin-photo" type="file" class="form-control-file login-field"
						       value="<?php echo(isset($_POST['sysin-photo']) ? $_POST['sysin-photo'] : null); ?>"
						       placeholder="" id="sysin-photo" capture="camera"/>
					</div>
				</div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-police">Policial <b>*</b></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div style="margin-top: 5px;">
                            <input type="checkbox" class="js-switch" id="sysin-police" name="sysin-police"
                                   value="1"/>
                        </div>
                    </div>
                </div>

                <div class="police-fields hidden">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-police-institution">Instituição</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="sysin-police-institution" class="form-control" name="sysin-police-institution">
                                <option value="">Escolha uma instituição</option>
                                <option value="pc">Polícia civil</option>
                                <option value="pm">Polícia militar</option>
                                <option value="pf">Polícia federal</option>
                                <option value="others">Outros</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-police-registration">Matrícula</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="sysin-police-registration" type="text" class="form-control login-field"
                                   value="<?php echo(isset($_POST['sysin-police-registration']) ? $_POST['sysin-police-registration'] : null); ?>"
                                   placeholder="" id="sysin-police-registration"/>
                        </div>
                    </div>

                    <!--<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-police-armed">Armado <b>*</b></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div style="margin-top: 5px;">
                                <input type="checkbox" class="js-switch" id="sysin-police-armed" name="sysin-police-armed"
                                       value="<?php echo(isset($_POST['sysin-police-armed']) ? $_POST['sysin-police-armed'] : null); ?>"/>
                            </div>
                        </div>
                    </div>-->

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sysin-police-weapon-carrying-number">Nº do porte</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="sysin-police-weapon-carrying-number" type="text" class="form-control login-field"
                                   value="<?php echo(isset($_POST['sysin-police-weapon-carrying-number']) ? $_POST['sysin-police-weapon-carrying-number'] : null); ?>"
                                   placeholder="" id="sysin-police-weapon-carrying-number"/>
                        </div>
                    </div>
                </div>

			</div>
    </div>
	<div class="modal-footer">
		<button class="btn btn-primary" type="button" data-dismiss="modal">Cancelar</button>
        <input type="hidden" name="sysin-user-id" id="sysin-user-id" value="">
		<input type="submit" name="sysin-submit" id="sysin-submit" class="btn btn-success" value="Cadastrar cliente">
	</div>
	</form>

	<?php return ob_get_clean();
}

function validation() {
	$name = $_POST['sysin-name'];
	$surname = $_POST['sysin-surname'];
	$email = $_POST['sysin-email'];
	$cpf = $_POST['sysin-cpf'];
	/* $gender = $_POST['sysin-gender'];
	$birthday = $_POST['sysin-birthday'];
	$photo = $_POST['sysin-photo']; */

	// Mandatory fields
	if (empty($name) || empty($surname) || empty($cpf) ) {
		return new WP_Error('field', 'Todos os campos são de preenchimento obrigatório.');
	}

	// Invalid email (if not empty)
	if (!empty($email)) {
		if (!is_email($email)) {
			return new WP_Error('email_invalid', 'O email parece ser inválido');
		}
	}

	// Email already in use
	if (email_exists($email)) {
		return new WP_Error('email', 'Este email já sendo utilizado.');
	}

	// Invalid CPF
	if (strlen( str_replace('.', '',  str_replace('-', '', $cpf) ) ) !== 11) {
		return new WP_Error('cpf', 'O CPF digitado é inválido.');
	}

	// Check if CPF is in use
	if (username_exists($cpf)) {
		return new WP_Error('existing_user_login', 'Este CPF já sendo utilizado.');
	}
}

function registration() {
	$name = $_POST['sysin-name'];
	$surname = $_POST['sysin-surname'];
	$email = $_POST['sysin-email'];
	$cpf = $_POST['sysin-cpf'];
	$gender = $_POST['sysin-gender'];
	$birthday = convert_date_to_sql_format ($_POST['sysin-birthday'] );
	$photo = $_FILES['sysin-photo'];
	$police = $_POST['sysin-police'];
	$police_institution = $_POST['sysin-police-institution'];
	$police_registration = $_POST['sysin-police-registration'];
	$police_weapon_carrying_number = $_POST['sysin-police-weapon-carrying-number'];

	$userdata = array(
		'first_name' => esc_attr($name),
		'last_name' => esc_attr($surname),
		'display_name' => esc_attr($name),
		'user_login' => esc_attr($cpf),
		'user_email' => ( empty($email) ) ? esc_attr($cpf) . '@sysin.com' : esc_attr($email),
		'user_pass' => esc_attr($cpf)
	);

	$response = array(
        'success' => false,
        'message' => ''
    );

	$errors = validation();

	if ( is_wp_error( $errors ) ) {

		$response['message']  = '<div class="alert alert-danger text-center" data-error="' . $errors->get_error_code() . '">';
		$response['message'] .= '<strong>' . $errors->get_error_message() . '</strong>';
		$response['message'] .= '</div>';

		echo json_encode($response);

	} else {
		$register_user = wp_insert_user($userdata);
		if ( !is_wp_error($register_user) ) {
			add_user_meta( $register_user, '_user_cpf', esc_attr($cpf), true );
			add_user_meta( $register_user, '_user_gender', esc_attr($gender), true );
			add_user_meta( $register_user, '_user_birthday', esc_attr($birthday), true );
			add_user_meta( $register_user, '_user_police', esc_attr($police), true );
			add_user_meta( $register_user, '_user_police_institution', esc_attr($police_institution), true );
			add_user_meta( $register_user, '_user_police_registration', esc_attr($police_registration), true );
			add_user_meta( $register_user, '_user_police_weapon_carrying_number', esc_attr($police_weapon_carrying_number), true );

			client_photo($photo, $register_user);

			$response['success'] = true;
			$response['success'] = true;
			$response['message']  = '<div class="alert alert-success text-center">';
			$response['message'] .= '<strong>Cadastro realizado com sucesso</strong>';
			$response['message'] .= '</div>';

			echo json_encode($response);

		} else {
			$response['message']  = '<div class="alert alert-danger text-center" data-error="' . $register_user->get_error_code() . '">';
			$response['message'] .= '<strong>' . $register_user->get_error_message() . '</strong>';
			$response['message'] .= '</div>';

			echo json_encode($response);
		}
	}

}

function editing() {
	$user_id = $_POST['sysin-user-id'];
	$name = $_POST['sysin-name'];
	$surname = $_POST['sysin-surname'];
	$email = $_POST['sysin-email'];
	$cpf = $_POST['sysin-cpf'];
	$gender = $_POST['sysin-gender'];
	$birthday = convert_date_to_sql_format ($_POST['sysin-birthday'] );
	$photo = $_FILES['sysin-photo'];
	$police = $_POST['sysin-police'];
	$police_institution = $_POST['sysin-police-institution'];
	$police_registration = $_POST['sysin-police-registration'];
	$police_weapon_carrying_number = $_POST['sysin-police-weapon-carrying-number'];

	$userdata = array(
		'ID' => $user_id,
		'first_name' => esc_attr($name),
		'last_name' => esc_attr($surname),
		'display_name' => esc_attr($name),
		'user_login' => esc_attr($cpf),
		'user_email' => ( empty($email) ) ? esc_attr($cpf) . '@sysin.com' : esc_attr($email),
		'user_pass' => esc_attr($cpf)
	);

	$response = array(
		'success' => false,
		'message' => ''
	);

	$user_id = wp_update_user( $userdata );

	if ( is_wp_error( $user_id ) ) {
		$response['message']  = '<div class="alert alert-danger text-center" data-error="' . $user_id->get_error_code() . '">';
		$response['message'] .= '<strong>' . $user_id->get_error_message() . '</strong>';
		$response['message'] .= '</div>';
		echo json_encode($response);

	} else {
		update_user_meta( $user_id, '_user_cpf', esc_attr($cpf) );
		update_user_meta( $user_id, '_user_gender', esc_attr($gender) );
		update_user_meta( $user_id, '_user_birthday', esc_attr($birthday) );
		update_user_meta( $user_id, '_user_police', esc_attr($police) );
		update_user_meta( $user_id, '_user_police_institution', esc_attr($police_institution) );
		update_user_meta( $user_id, '_user_police_registration', esc_attr($police_registration) );
		update_user_meta( $user_id, '_user_police_weapon_carrying_number', esc_attr($police_weapon_carrying_number) );

		client_photo($photo, $user_id);

		$response['success'] = true;
		$response['message']  = '<div class="alert alert-success text-center">';
		$response['message'] .= '<strong>Cliente atualizado com sucesso</strong>';
		$response['message'] .= '</div>';
		echo json_encode($response);

	}
}

function delete_client ($client_id) {
	$response['success'] = false;
	require_once(ABSPATH.'wp-admin/includes/user.php' );

	if( wp_delete_user( $client_id ) ) {
		$response['success'] = true;
		$response['message']  = '<div class="alert alert-success text-center">';
		$response['message'] .= '<strong>Cliente deletado com sucesso</strong>';
		$response['message'] .= '</div>';
		echo json_encode($response);
    } else {
		$response['message']  = '<div class="alert alert-danger text-center">';
		$response['message'] .= '<strong>Ocorreu um erro durante a exclusão do usuário.</strong>';
		$response['message'] .= '</div>';
		echo json_encode($response);
    }
}


add_action('wp_ajax_register_client', 'sysin_register_client');
add_action('wp_ajax_nopriv_register_client', 'sysin_register_client');
function sysin_register_client() {
	if ( $_POST['sysin-submit'] ) {
		validation();
		registration();
	}
	die();
}

add_action('wp_ajax_edit_client', 'sysin_edit_client');
add_action('wp_ajax_nopriv_edit_client', 'sysin_edit_client');
function sysin_edit_client() {
	if ( $_POST['sysin-submit'] ) {
		// validation();
		editing();
	}
	die();
}

add_action('wp_ajax_delete_client', 'sysin_delete_client');
add_action('wp_ajax_nopriv_delete_client', 'sysin_delete_client');
function sysin_delete_client() {
	if ( !empty($_POST['user_id']) ) {
		delete_client( $_POST['user_id'] );
	}
	die();
}

add_action('wp_ajax_get_clients', 'sysin_get_clients');
add_action('wp_ajax_nopriv_get_clients', 'sysin_get_clients');
function sysin_get_clients() {
	$args          = array( 'role' => 'Subscriber' );
	$clients_query = new WP_User_Query( $args );

	if ( ! empty( $clients_query->get_results() ) ) :
		$clients = array();
		foreach ( $clients_query->get_results() as $i => $client ) :

			$clients[$i]                                         = array();
			$clients[$i]['client-id']                            = $client->ID;
			$clients[$i]['client-name']                          = $client->first_name;
			$clients[$i]['client-surname']                       = $client->last_name;
			$clients[$i]['client-email']                         = $client->user_email;
			$clients[$i]['client-cpf']                           = get_user_meta( $client->ID, '_user_cpf', true );
			$clients[$i]['client-gender']                        = get_user_meta( $client->ID, '_user_gender', true );
			$clients[$i]['client-birthday']                      = get_user_meta( $client->ID, '_user_birthday', true );
			$clients[$i]['client-photo']                         = get_user_meta( $client->ID, '_user_photo', false );
			$clients[$i]['client-police']                        = get_user_meta( $client->ID, '_user_police', true );
			$clients[$i]['client-police-institucion']            = get_user_meta( $client->ID, '_user_police_institution', true );
			$clients[$i]['client-police-registration']           = get_user_meta( $client->ID, '_user_police_registration', true );
			$clients[$i]['client-police-armed']                  = get_user_meta( $client->ID, '_user_police_armed', true );
			$clients[$i]['client-police-weapon-carrying-number'] = get_user_meta( $client->ID, '_user_police_weapon_carrying_number', true );
		endforeach;

		echo json_encode($clients);

    endif;
	die();
}

add_action('wp_ajax_get_clients_table', 'sysin_get_clients_table');
add_action('wp_ajax_nopriv_get_clients_table', 'sysin_get_clients_table');
function sysin_get_clients_table() {
	include get_template_directory() . '/inc/clients-table.php';
	die();
}

add_action('wp_ajax_client_checkin', 'sysin_client_checkin');
add_action('wp_ajax_nopriv_client_checkin', 'sysin_client_checkin');
function sysin_client_checkin() {

	$response['success'] = false;
    if( !empty($_POST['user_id']) ){
	    if( add_user_meta($_POST['user_id'], '_client_checkin', convert_date_to_sql_format( $_POST['checkin'], true ), false) ) {
		    $response['success'] = true;
		    $response['message']  = '<div class="alert alert-success text-center">';
		    $response['message'] .= '<strong>Check-in realizado!</strong>';
		    $response['message'] .= '</div>';
        };
    }

	echo json_encode($response);
	die();
}