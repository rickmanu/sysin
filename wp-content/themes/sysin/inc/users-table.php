<?php

/** Loads the WordPress Environment and Template */
if( !empty($_POST['get-users-table-ajax']) ) {
	require( dirname( __FILE__ ) . '../../../../../wp-blog-header.php' );
}

?>
<table id="users-datatable" class="table table-striped table-responsive table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
	<tr>
		<th>Login</th>
		<th>Nome</th>
		<th>Sobrenome</th>
		<th>E-mail</th>
		<th>CPF</th>
		<th>Sexo</th>
		<th>Data de nascimento</th>
		<th>Permissão</th>
		<th>Fotos</th>
		<th>Ações</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$args = array( 'role__not_in' => 'Subscriber' );
	$users_query = new WP_User_Query( $args );
	if ( ! empty( $users_query->get_results() ) ) :
		foreach ( $users_query->get_results() as $user ) : ?>
			<tr>
				<td>
					<b><?php echo $user->user_login; ?></b>
				</td>
				<td>
                    <b><?php echo $user->first_name; ?></b>
				</td>
				<td>
                    <b><?php echo $user->last_name; ?></b>
				</td>
				<td>
                    <b><?php echo $user->user_email; ?></b>
				</td>
                <td>
                    <b><?php echo get_user_meta( $user->ID, '_user_cpf', true); ?></b>
                </td>
                <td>
                    <b><?php echo get_user_meta( $user->ID, '_user_gender', true); ?></b>
                </td>
                <td>
                    <b><?php echo get_user_meta( $user->ID, '_user_birthday', true); ?></b>
                </td>
                <td>
                    <b><?php echo $user->roles[0]; ?></b>
                </td>
                <td class="text-center">
                    <?php
                    $client_photo = get_user_meta($user->ID, '_user_photo', false);
                    $client_images = count( $client_photo );
                    $client_image = wp_get_attachment_image ( end( $client_photo ), 'client-thumb');
                    $client_image_original = wp_get_attachment_image_src ( end( $client_photo ), 'large'); ?>

                    <?php if( $client_image ) : ?>
                        <a class="btn btn-app" data-fancybox="group-<?php echo $user->ID; ?>" href="<?php echo ( $client_image_original ) ? $client_image_original[0] : ''; ?>">
                            <?php echo ( $client_images > 1 ) ? '<span class="badge bg-green">' . $client_images . '</span>' : ''; ?>
                            <?php echo $client_image; ?>
                        </a>
                    <?php else: ?>
                        <a class="btn btn-app">
                            <i class="fa fa-camera"></i>
                        </a>
                    <?php endif; ?>

                    <?php
                    $client_photo = array_pop ( $client_photo );
                    foreach ( $client_photo as $attachment ){
                        $client_image = wp_get_attachment_image_src ( $attachment, 'large');

                        echo '<a href="'. $client_image[0] .'" data-fancybox="group-'. $user->ID .'"></a>';
                    } ?>
                </td>
				<td class="text-center">
                    <a href="#"
                       class="btn btn-info edit-client"
                       data-toggle="modal"
                       data-target="#users-modal"
                       data-is-editing="true"
                       data-client-id="<?php echo $user->ID; ?>"
                       data-client-login="<?php echo $user->user_login; ?>"
                       data-client-name="<?php echo $user->first_name; ?>"
                       data-client-surname="<?php echo $user->last_name; ?>"
                       data-client-email="<?php echo $user->user_email; ?>"
                       data-client-cpf="<?php echo get_user_meta( $user->ID, '_user_cpf', true); ?>"
                       data-client-gender="<?php echo get_user_meta( $user->ID, '_user_gender', true); ?>"
                       data-client-birthday="<?php echo get_user_meta( $user->ID, '_user_birthday', true); ?>"
                       data-client-roles="<?php echo $user->roles[0]; ?>">
                        <i class="fa fa-pencil"></i> <span class="hide">Editar dados</span>
                    </a>
                    <a data-toggle="modal"
                       data-target="#delete-user-modal"
                       data-client-id="<?php echo $user->ID; ?>"
                       data-client-login="<?php echo $user->user_login; ?>"
                       class="delete-client btn btn-danger">
                        <i class="fa fa-trash-o"></i> <span class="hide">Excluir</span>
                    </a>
				</td>
			</tr>
		<?php endforeach;
	else: ?>
		<tr>
			<td colspan="8"><b>Nenhum cliente encontrado.</b></td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>