<?php
global $wpdb;
$args = array(
	'role' => 'Subscriber',
	'count_total' => true
);
$clients = new WP_User_Query( $args );
$clients_male = $wpdb->query( "SELECT DISTINCT user_id FROM {$wpdb->prefix}usermeta WHERE meta_value =  'male'" );
$clients_female = $wpdb->query( "SELECT DISTINCT user_id FROM {$wpdb->prefix}usermeta WHERE meta_value =  'female'" );
$checkins_total = $wpdb->query( "SELECT t.* FROM {$wpdb->prefix}usermeta t WHERE meta_key = '_client_checkin'" );

$date_minus_a_week = date('Y-m-d', strtotime('-7 days') );
$clients_last_week_sql   = "SELECT SQL_CALC_FOUND_ROWS {$wpdb->prefix}users.* 
                            FROM   {$wpdb->prefix}users 
                            WHERE  1 = 1 
                                   AND ( {$wpdb->prefix}users.user_registered >= '$date_minus_a_week 00:00:00' 
                                         AND id IN (SELECT DISTINCT user_id 
                                                    FROM   {$wpdb->prefix}usermeta 
                                                    WHERE  meta_value = 'a:1:{s:10:\"subscriber\";b:1;}') ) 
                            ORDER  BY user_login ASC;";
$clients_last_week = $wpdb->query($clients_last_week_sql);

$args = array(
	'role__in' => array('Administrator', 'Editor'),
	'count_total' => true
);
$admins = new WP_User_Query( $args );

?>
<div id="stats-panel" class="row tile_count">
	<div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
		<span class="count_top"><i class="fa fa-user"></i> Total de clientes</span>
		<div class="count green"><?php echo $clients->get_total(); ?></div>
        <?php if( $clients_last_week ) : ?>
		<span class="count_bottom"><i class="green"><i
					class="fa fa-sort-asc"></i>+<?php echo $clients_last_week; ?> </i> na última semana</span>
        <?php endif; ?>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <span class="count_top"><i class="fa fa-clock-o"></i> Check-ins realizados</span>
		<div class="count"><?php echo $checkins_total; ?></div>
		<span class="count_bottom"><i class="green"><i
					class="fa fa-sort-asc"></i>+3 </i> na última semana</span>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
		<span class="count_top"><i class="fa fa-male"></i> Homens</span>
		<div class="count"><?php echo $clients_male; ?></div>
		<span class="count_bottom"><i class="green"><?php echo round( ( $clients_male / $clients->get_total() * 100), 2 ); ?>% </i> do total</span>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
		<span class="count_top"><i class="fa fa-female"></i> Mulheres</span>
		<div class="count"><?php echo $clients_female; ?></div>
		<span class="count_bottom"><i class="red"><?php echo round( ( $clients_female / $clients->get_total() * 100), 2); ?>% </i> do total</span>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
		<span class="count_top"><i class="fa fa-heart"></i> Média de idade</span>
		<div class="count">26</div>
		<!--<span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> Ipsum lorem</span>-->
	</div>
	<div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
		<span class="count_top"><i class="fa fa-user"></i> Administradores do sistema</span>
		<div class="count"><?php echo $admins->get_total(); ?></div>
		<!--<span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> Lorem lorenado</span>-->
	</div>
</div>