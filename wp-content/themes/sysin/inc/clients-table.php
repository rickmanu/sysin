<?php

/** Loads the WordPress Environment and Template */
if( !empty($_POST['get-clients-table-ajax']) ) {
	require( dirname( __FILE__ ) . '../../../../../wp-blog-header.php' );
}

?>
<table id="clients-datatable" class="table table-striped table-responsive table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
	<tr>
		<th data-priority="1">Nome</th>
		<th>E-mail</th>
		<th data-priority="2">CPF</th>
		<th>Data de nascimento</th>
		<th>Último check-in</th>
		<th data-priority="3">Foto</th>
		<th data-priority="0">Ações</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$args = array( 'role' => 'Subscriber' );
	$clients_query = new WP_User_Query( $args );
	if ( ! empty( $clients_query->get_results() ) ) :
		foreach ( $clients_query->get_results() as $client ) :

			$clientsData = array();
			$clientsData['client-id'] = $client->ID;
			$clientsData['client-name'] = $client->first_name;
			$clientsData['client-surname'] = $client->last_name;
			$clientsData['client-email'] = $client->user_email;
			$clientsData['client-cpf'] = get_user_meta($client->ID, '_user_cpf', true);
			$clientsData['client-gender'] = get_user_meta($client->ID, '_user_gender', true);
			$clientsData['client-birthday'] = date('d/m/Y', strtotime( get_user_meta($client->ID, '_user_birthday', true) ) );
			$clientsData['client-photo'] = get_user_meta($client->ID, '_user_photo', false);
			$clientsData['client-police'] = get_user_meta($client->ID, '_user_police', true);
			$clientsData['client-police-institucion'] = get_user_meta($client->ID, '_user_police_institution', true);
			$clientsData['client-police-registration'] = get_user_meta($client->ID, '_user_police_registration', true);
			$clientsData['client-police-armed'] = get_user_meta($client->ID, '_user_police_armed', true);
			$clientsData['client-police-weapon-carrying-number'] = get_user_meta($client->ID, '_user_police_weapon_carrying_number', true);
			$clientsData['client-checkins'] = get_user_meta($client->ID, '_client_checkin', false);

			?>
			<tr>
				<td>
					<b><?php echo $clientsData['client-name']; ?></b>
				</td>
				<td>
					<?php echo $clientsData['client-email']; ?>
				</td>
				<td>
					<?php echo $clientsData['client-cpf']; ?>
				</td>
				<td>
					<?php echo $clientsData['client-birthday']; ?>
				</td>
				<td>
					<?php
					if( !empty($clientsData['client-checkins']) ){
					    echo date('d/m/Y H:i', strtotime( end( $clientsData['client-checkins'] ) ) );
                    } else {
					    echo 'Ainda não foram realizados check-ins.';
                    }
					?>
				</td>
				<td class="text-center">
					<?php
					$client_images = count( $clientsData['client-photo'] );
					$client_image = wp_get_attachment_image ( end( $clientsData['client-photo'] ), 'client-thumb');
					$client_image_original = wp_get_attachment_image_src ( end( $clientsData['client-photo'] ), 'large'); ?>

                    <?php if( $client_image ) : ?>
					<a class="btn btn-app" data-fancybox="group-<?php echo $clientsData['client-id']; ?>" href="<?php echo ( $client_image_original ) ? $client_image_original[0] : ''; ?>">
						<?php echo ( $client_images > 1 ) ? '<span class="badge bg-green">' . $client_images . '</span>' : ''; ?>
                        <?php echo $client_image; ?>
					</a>
                    <?php else: ?>
                    <a class="btn btn-app">
                        <i class="fa fa-camera"></i>
                    </a>
                    <?php endif; ?>

					<?php
                    $client_photo = array_pop ( $clientsData['client-photo'] );
                    foreach ( $clientsData['client-photo'] as $attachment ){
						$client_image = wp_get_attachment_image_src ( $attachment, 'large');

						echo '<a href="'. $client_image[0] .'" data-fancybox="group-'. $clientsData['client-id'] .'"></a>';
					} ?>
				</td>
				<td class="text-center">
					<a href="#"
                       data-toggle="modal"
					   data-target="#client-checkin-modal"
                       data-client-id="<?php echo $clientsData['client-id']; ?>"
                       data-client-name="<?php echo $clientsData['client-name']; ?>"
                       data-client-surname="<?php echo $clientsData['client-surname']; ?>"
                       data-client-cpf="<?php echo $clientsData['client-cpf']; ?>"
                       data-client-photo="<?php $photo = wp_get_attachment_image_src ( end( $clientsData['client-photo'] ), 'thumbnail'); echo $photo[0]; ?>"
                       data-client-birthday="<?php echo $clientsData['client-birthday']; ?>"
                       data-client-checkins="<?php echo str_replace('"', "'", json_encode( $clientsData['client-checkins']) ); ?>"
					   class="btn btn-success">
						<i class="fa fa-check-square-o"></i> <span class="hide">Cadastrar check-in</span>
					</a>
					<a href="#"
					   class="btn btn-info edit-client"
					   data-toggle="modal"
					   data-target="#register-client-modal"
						<?php foreach ($clientsData as $i => $clientData ){
							echo 'data-' . $i .'="'. $clientData . '" ';
						} ?>>
						<i class="fa fa-pencil"></i> <span class="hide">Editar dados</span>
					</a>
					<a href="#" data-toggle="modal"
					   data-target="#delete-client-modal"
					   data-client-id="<?php echo $clientsData['client-id']; ?>"
					   data-client-name="<?php echo $clientsData['client-name']; ?>"
					   data-client-surname="<?php echo $clientsData['client-surname']; ?>"
					   data-client-cpf="<?php echo $clientsData['client-cpf']; ?>"
					   class="delete-client btn btn-danger">
						<i class="fa fa-trash-o"></i> <span class="hide">Excluir</span>
					</a>
				</td>
			</tr>
		<?php endforeach;
	else: ?>
		<tr>
			<td colspan="8"><b>Nenhum cliente encontrado.</b></td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>