'use strict';

/**
 * Defining base paths
 *
 */
var basePaths = {
    node: './node_modules/',                                        // Path to node packages
    projectPHPFiles: './**/*.php',                                  // Path to all PHP files.
    projectStylesheetFiles: './assets/css/',   // Path to all *.scss files inside css folder and inside them.
    projectJSFiles: './assets/js/'                          // Path to all custom JS files.
};

/**
 * Defining requirements
 *
 */
var gulp = require('gulp'),
    uglify = require('gulp-uglify-es').default,
    sassVariables = require('gulp-sass-variables'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    sourcemaps = require('gulp-sourcemaps'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync').create(),
    livereload = require('gulp-livereload');

/**
 * Configure the javascript bundle for the application
 *
 */
gulp.task('scripts', function () {
    return gulp.src([
        // @TODO The current version of bootstrap (4.0.0-beta.2) is not working alongside WP jQuery
        // basePaths.node + 'popper.js/dist/umd/popper.js',
        // basePaths.node + 'jquery/dist/jquery.js',
        // basePaths.node + 'bootstrap/dist/js/bootstrap.min.js',
        /* basePaths.projectJSFiles + 'vendor/popper.js',
        basePaths.projectJSFiles + 'vendor/bootstrap.js',
        basePaths.projectJSFiles + 'vendor/*.js', */
        basePaths.node + 'gentelella/vendors/bootstrap/dist/js/bootstrap.min.js',
        basePaths.node + 'gentelella/vendors/fastclick/lib/fastclick.js',
        basePaths.node + 'gentelella/vendors/nprogress/nprogress.js',
        basePaths.node + 'gentelella/vendors/iCheck/icheck.min.js',
        basePaths.node + 'gentelella/vendors/moment/min/moment.min.js',
        basePaths.node + 'gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        basePaths.node + 'gentelella/vendors/switchery/dist/switchery.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net/js/jquery.dataTables.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
        basePaths.node + 'gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js',
        basePaths.node + 'gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js',

        basePaths.projectJSFiles + 'jquery.fancybox.js',
        basePaths.projectJSFiles + 'pt-br.js',
        basePaths.projectJSFiles + 'jquery.mask.js',
        basePaths.projectJSFiles + '*.js'
    ])
        .pipe(plumber())
        .pipe(concat('bundle.min.js'))
        .pipe(sourcemaps.init())
        // .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./assets/dist/js/'))
        .pipe(notify('Task JS finished!'))
        .pipe(livereload());
});

/**
 * Configure the stylesheet bundle for the application
 *
 */
gulp.task('styles', function () {
    return gulp.src([
        // basePaths.node + 'bootstrap/scss/bootstrap.scss',
        // basePaths.node + 'font-awesome/scss/font-awesome.scss',
        basePaths.node + 'gentelella/vendors/bootstrap/dist/css/bootstrap.css',
        // basePaths.node + 'gentelella/vendors/font-awesome/css/font-awesome.min.css',
        basePaths.node + 'gentelella/vendors/nprogress/nprogress.css',
        basePaths.node + 'gentelella/vendors/iCheck/skins/flat/green.css',
        basePaths.node + 'gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css',
        basePaths.node + 'gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
        basePaths.node + 'gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
        basePaths.node + 'gentelella/vendors/switchery/dist/switchery.min.css',
        basePaths.node + 'gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
        basePaths.node + 'gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
        basePaths.node + 'gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
        basePaths.node + 'gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
        basePaths.node + 'gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
        // basePaths.projectStylesheetFiles + 'bootstrap.css',
        basePaths.projectStylesheetFiles + 'font-awesome.css',
        basePaths.projectStylesheetFiles + 'jquery.fancybox.css',
        basePaths.projectStylesheetFiles + 'main.css'
    ])
        /* .pipe(sassVariables({
            // Set variables for define font-awesome fonts folder
            '$fa-font-path': '../../fonts'
        }))*/
        .pipe(plumber())
        .pipe(sourcemaps.init())
        // .pipe(sass())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('bundle.min.css'))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('./assets/dist/css/'))
        .pipe(notify('Task CSS finished!'))
        .pipe(livereload());
});

/**
 * Copy fonts files to distribution
 *
 */
gulp.task('font-awesome', function () {
    gulp.src([
        basePaths.node + 'font-awesome/fonts/**/*.{otf,eot,svg,ttf,woff,woff2,eof}'
    ]).pipe(gulp.dest('./assets/dist/fonts/'));

    gulp.src([
        basePaths.node + 'font-awesome/scss/font-awesome.scss'
    ])
        .pipe(sassVariables({
            // Set variables for define font-awesome fonts folder
            '$fa-font-path': '../../../wp-content/themes/sysin/assets/fonts'
        }))
        .pipe(sass())
        .pipe(gulp.dest('./assets/css/'))

});

/**
 * Watch for PHP changes
 *
 */
gulp.task('php', function () {
    gulp.src(basePaths.projectPHPFiles).pipe(livereload());
});

/**
 * Synchronised browser testing
 *
 */
gulp.task('browser-sync', function() {
    browserSync.init([
        basePaths.projectPHPFiles,
        basePaths.projectStylesheetFiles,
        basePaths.projectJSFiles
    ]);
});

/**
 * Watch for changes
 *
 */
gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('./assets/css/**/*.css', ['styles']);
    gulp.watch('./assets/js/*.js', ['scripts']);
    gulp.watch('**/*.php', ['php']);
});

/**
 * Default task
 *
 */
gulp.task('default', ['font-awesome', 'styles', 'scripts', 'watch']);