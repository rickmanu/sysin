(function ($) {
    var $BODY = $('body'),
        $MENU_TOGGLE = $('#menu_toggle'),
        $SIDEBAR_MENU = $('#sidebar-menu'),
        $CLIENTS_TABLE = $('#clients-datatable'),
        $USERS_TABLE = $('#users-datatable'),
        $SIDEBAR_FOOTER = $('.sidebar-footer'),
        $LEFT_COL = $('.left_col'),
        $RIGHT_COL = $('.right_col'),
        $NAV_MENU = $('.nav_menu'),
        $FOOTER = $('footer'),
        dataTableOptions = {
            "responsive": true,
            "language": {
                "sEmptyTable": "Nenhum cliente encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de um total de _TOTAL_ clientes",
                "sInfoEmpty": "Nenhum cliente",
                "sInfoFiltered": "",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ Clientes por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum cliente encontrado",
                "sSearch": "Pesquisar por cliente",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }
        };

    $(document).ready(function () {
        app.init();
    });

    var app = {
        init: function () {
            this.initDatatables();
            this.utils();
            this.clientMainForm();
            this.editClient();
            this.deleteClient();
            this.clientCheckin();
            this.editUser();
            this.deleteUser();
            this.openModalBasedOnURL();
        },

        utils: function () {
            $SIDEBAR_MENU.find('a').on('click', function(ev) {
                console.log('clicked - sidebar_menu');
                var $li = $(this).parent();

                if ($li.is('.active')) {
                    $li.removeClass('active active-sm');
                    $('ul:first', $li).slideUp(function() {
                        setContentHeight();
                    });
                } else {
                    // prevent closing menu if we are on child menu
                    if (!$li.parent().is('.child_menu')) {
                        $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                        $SIDEBAR_MENU.find('li ul').slideUp();
                    }else
                    {
                        if ( $BODY.is( ".nav-sm" ) )
                        {
                            $SIDEBAR_MENU.find( "li" ).removeClass( "active active-sm" );
                            $SIDEBAR_MENU.find( "li ul" ).slideUp();
                        }
                    }
                    $li.addClass('active');

                    $('ul:first', $li).slideDown(function() {
                        setContentHeight();
                    });
                }
            });

            $MENU_TOGGLE.on('click', function() {
                console.log('clicked - menu toggle');

                if ($BODY.hasClass('nav-md')) {
                    $SIDEBAR_MENU.find('li.active ul').hide();
                    $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
                } else {
                    $SIDEBAR_MENU.find('li.active-sm ul').show();
                    $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
                }

                $BODY.toggleClass('nav-md nav-sm');

                // setContentHeight();

                $('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
            });

            if ($(".js-switch")[0]) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html, {
                        color: '#26B99A'
                    });
                });
            }

            if ($("input.flat")[0]) {
                $(document).ready(function () {
                    $('input.flat').iCheck({
                        checkboxClass: 'icheckbox_flat-green',
                        radioClass: 'iradio_flat-green'
                    });
                });
            }

            // Instatiate datepicker plugin
            $('#sysin-birthday').datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'pt-br',
                viewMode: 'years'
            });


            // Default value for check-in field
            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();

            var output = ( day < 10 ? '0' : '') + day + '/' + ( month < 10 ? '0' : '') + month + '/' + d.getFullYear() + ' ' + ( d.getHours() < 10 ? '0' : '') + d.getHours() + ':' + ( d.getMinutes() < 10 ? '0' : '') + d.getMinutes();

            $('#client-checkin-wrapper').datetimepicker({
                format: 'DD/MM/YYYY HH:mm',
                locale: 'pt-br',
                minDate: moment(),
                widgetPositioning: {
                    vertical: 'bottom',
                    horizontal: 'auto'
                }
            }).on('dp.show', function() {
                return $(this).data('DateTimePicker').defaultDate(new Date());
            });

            $('#client-checkin').val(output);

            // Masks
            $('.cpf').mask('000.000.000-00', {reverse: true});

        },

        clientMainForm: function () {
            $('#sysin-register-form').submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                if( $(this).data('editing-client') ){
                    formData.append('action', 'edit_client');
                } else {
                    formData.append('action', 'register_client');
                }
                formData.append('sysin-submit', true);

                $.ajax({
                    url : sysin.ajaxUrl,
                    data : formData,
                    type : 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        // console.log('Registrando cliente');
                        $('#sysin-register-form > .modal-body > .row .alert').remove();
                        $('#sysin-register-form :input').attr('disabled', true);
                    },
                    success : function( data ){
                        data = JSON.parse(data);
                        if( data.success === true ){
                            // console.log('Cliente registrado com sucesso!');
                            $('#sysin-register-form :input').removeAttr('disabled');
                            $('#sysin-register-form > .modal-body > .row').prepend(data.message);

                            if( !$('#sysin-register-form').data('editing-client') ){
                                $('#sysin-register-form').trigger('reset');
                            }
                            app.redrawClientsDataTable();
                        } else {
                            $('#sysin-register-form > .modal-body > .row .alert').remove();
                            $('#sysin-register-form :input').removeAttr('disabled');
                            $('#sysin-register-form > .modal-body > .row').prepend(data.message);
                        }
                    },
                    error: function(error){
                        console.error( 'ERROR: ', error );
                    }
                });
            });

            $('#sysin-police').on('change', function () {
                if( $(this).is(':checked') ){
                  $('.police-fields').removeClass('hidden');
                } else {
                    $('.police-fields').addClass('hidden');
                }
            })
        },

        editClient: function () {
            $('#register-client-modal').on('show.bs.modal', function (event) {
                if( $(event.relatedTarget).hasClass('edit-client') ){

                    $('#register-client-modal .modal-title').text('Edição de cliente');
                    $('#sysin-submit').val('Atualizar cliente');
                    $('#sysin-register-form').attr('data-editing-client', true);
                    $('#register-client-modal .modal-body .alert').remove();

                    var button = $(event.relatedTarget),
                        clientID = button.data('client-id'),
                        clientName = button.data('client-name'),
                        clientSurname = button.data('client-surname'),
                        clientEmail = button.data('client-email'),
                        clientCPF = button.data('client-cpf'),
                        clientGender = button.data('client-gender'),
                        clientBirthday = button.data('client-birthday'),
                        clientPolice = button.data('client-police'),
                        clientPoliceInstitucion = button.data('client-police-institucion'),
                        clientPoliceRegistration = button.data('client-police-registration'),
                        clientPoliceArmed = button.data('client-police-armed'),
                        clientPoliceWeaponCarryingNumber = button.data('client-police-weapon-carrying-number');

                    var modal = $(this)
                        modal.find('#sysin-name').val(clientName)
                        modal.find('#sysin-surname').val(clientSurname)
                        modal.find('#sysin-email').val(clientEmail)
                        modal.find('#sysin-cpf').val(clientCPF)
                        modal.find('#sysin-gender [value="'+ clientGender +'"]').parent().addClass('active')
                        modal.find('#sysin-birthday').val(clientBirthday)
                        modal.find('#sysin-police-institution').val(clientPoliceInstitucion)
                        modal.find('#sysin-police-registration').val(clientPoliceRegistration)
                        modal.find('#sysin-police-weapon-carrying-number').val(clientPoliceWeaponCarryingNumber),
                        modal.find('#sysin-user-id').val(clientID);

                    if( clientPolice ) {
                        modal.find('#sysin-police').trigger('click');
                    }

                    if( clientPoliceArmed ) {
                        modal.find('#sysin-police-armed').trigger('click');
                    }

                    if( !clientGender.length ){
                        modal.find('#sysin-gender').find('.active').removeClass('active');
                    }

                } else {
                    $('#register-client-modal .modal-title').text('Cadastro de cliente');
                    $('#sysin-submit').text('Cadastrar cliente');
                    $('#sysin-register-form').removeAttr('data-editing-client');
                    $('#sysin-register-form').trigger('reset');
                }
            });
        },

        deleteClient: function () {
            $('#delete-client-modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    clientID = button.data('client-id'),
                    clientName = button.data('client-name'),
                    clientSurname = button.data('client-surname'),
                    clientCPF = button.data('client-cpf');

                $('#delete-client-modal').find('.client-data').html(clientName + ' ' + clientSurname + ' (CPF: '+ clientCPF +')');

                $('#delete-client').on('click', function () {
                    clientData = {
                        action: 'delete_client',
                        user_id: clientID
                    };
                    $.ajax({
                        url : sysin.ajaxUrl,
                        data : clientData,
                        type : 'POST',
                        beforeSend: function () {
                            // console.log('Deletando cliente');
                        },
                        success : function( data ){
                            data = JSON.parse(data);
                            if( data.success === true ){
                                // console.log('Cliente registrado com sucesso!');
                                $('#delete-client-modal .modal-body').html(data.message);
                                app.redrawClientsDataTable();
                                setTimeout(function () {
                                    $('#delete-client-modal').modal('hide');
                                }, 3000);

                            } else {
                                $('#delete-client-modal .modal-body').html(data.message);
                            }
                        },
                        error: function(error){
                            console.error( 'ERROR: ', error );
                        }
                    });
                })
            });
        },

        clientCheckin: function () {
            $('#client-checkin-modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    clientID = button.data('client-id'),
                    clientName = button.data('client-name'),
                    clientSurname = button.data('client-surname'),
                    clientCPF = button.data('client-cpf'),
                    clientPhoto = button.data('client-photo'),
                    clientBirthday = button.data('client-birthday'),
                    clientCheckins = button.data('client-checkins');

                if( clientCheckins.length ){
                    clientCheckins = clientCheckins.replace(/'/g, '"', clientCheckins);
                    $('#client-checkin-modal .last-checkins tbody').html('');

                    $.each(JSON.parse(clientCheckins), function (i, data) {
                        d = data.split(' ');
                        var tr = '<tr>' +
                            '<th scope="row">'+ (i + 1) +'</th>' +
                            '<td>'+ d[0] +'</td>' +
                            '<td>'+ d[1] +'</td>' +
                            '</tr>';
                        $('#client-checkin-modal .last-checkins tbody').append(tr);
                    });

                } else {
                    $('#client-checkin-modal .last-checkins tbody').html('<tr><td class="no-checkins" colspan="3">Ainda não foram realizados check-ins.</td></tr>');
                }

                // clientPhoto = ( clientPhoto.length ) ? clientPhoto : 'http://sysin.localhost/wp-content/themes/sysin/assets/img/logo-calaf.png';
                if( clientPhoto.length ){
                    $('#client-checkin-modal .profile_img').removeClass('hidden');
                    $('#client-checkin-modal .profile_img').attr('src', clientPhoto);
                    $('#client-checkin-modal .client-avatar .fa').addClass('hidden');
                } else {
                    $('#client-checkin-modal .profile_img').addClass('hidden');
                    $('#client-checkin-modal .client-avatar .fa').removeClass('hidden');
                }
                $('#client-checkin-modal').find('.name').text(clientName + ' ' + clientSurname);
                $('#client-checkin-modal').find('.cpf').text(clientCPF);
                $('#client-checkin-modal').find('.birthday').text(clientBirthday);

                $('#do-client-checkin').on('click', function (e) {
                    e.preventDefault();
                    clientData = {
                        action: 'client_checkin',
                        user_id: clientID,
                        checkin: $('#client-checkin').val()
                    };
                    $.ajax({
                        url : sysin.ajaxUrl,
                        data : clientData,
                        type : 'POST',
                        beforeSend: function () {
                            $('#client-checkin-modal .last-checkins tbody .no-checkins').remove();
                        },
                        success : function( data ){
                            data = JSON.parse(data);
                            if( data.success === true ){
                                var newValue = $('#client-checkin').val();
                                d = newValue.split(' ');
                                var tr = '<tr>' +
                                    '<th scope="row">'+ $('#client-checkin-modal .last-checkins tbody tr').length +'</th>' +
                                    '<td>'+ d[0] +'</td>' +
                                    '<td>'+ d[1] +'</td>' +
                                    '</tr>';
                                $('#client-checkin-modal .last-checkins tbody').append(tr);

                                app.redrawClientsDataTable();
                                // console.log('Cliente registrado com sucesso!');
                            } else {

                            }
                        },
                        error: function(error){
                            console.error( 'ERROR: ', error );
                        }
                    });
                })
            });
        },

        initDatatables: function () {
            if (typeof($.fn.DataTable) === 'undefined') {
                return;
            }

            $CLIENTS_TABLE.DataTable(dataTableOptions);
            $USERS_TABLE.DataTable(dataTableOptions);

            $('.dataTables_length, .dataTables_filter').parent().addClass('col-xs-12 col-sm-6 col-md-6 text-center-xs');
            $('.dataTables_info').parent().addClass('col-xs-12 col-sm-5 col-md-5 text-center-xs');
            $('.clients-datatable_paginate').parent().addClass('col-xs-12 col-sm-7 col-md-7 text-center-xs');

        },

        redrawClientsDataTable: function () {
            $('#clients-datatable').DataTable().destroy();
            $('#clients-datatable-main-wrapper').html();
            $.when(
                $.ajax({
                    url : sysin.templatePath + '/inc/clients-table.php',
                    data : {
                        'get-clients-table-ajax': true
                    },
                    type : 'POST'
                })
            ).then(function( clientsData ) {
                $('#clients-datatable-main-wrapper').html(clientsData);
                $('#clients-datatable').DataTable(dataTableOptions).draw();

                $('.dataTables_length, .dataTables_filter').parent().addClass('col-xs-12 col-sm-6 col-md-6 text-center-xs');
                $('.dataTables_info').parent().addClass('col-xs-12 col-sm-5 col-md-5 text-center-xs');
                $('.clients-datatable_paginate').parent().addClass('col-xs-12 col-sm-7 col-md-7 text-center-xs');
            });
        },

        editUser: function () {
            $('#users-modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    isEditing = button.data('is-editing'),
                    clientUserID = button.data('client-id'),
                    clientLogin = button.data('client-login'),
                    clientName = button.data('client-name'),
                    clientSurname = button.data('client-surname'),
                    clientEmail = button.data('client-email'),
                    clientCPF = button.data('client-cpf'),
                    clientGender = button.data('client-gender')
                    clientBirthday = button.data('client-birthday'),
                    clientRoles = button.data('client-roles');

                if( isEditing ){
                    $('#users-modal').find('.modal-title').text('Editar usuário');
                    $('#sysin-register-user-form').find('#sysin-submit').val('Atualizar usuário');
                    $('#sysin-register-user-form').find('#sysin-user-id').val(clientUserID);
                    $('#sysin-register-user-form').find('#sysin-login').val(clientLogin);
                    $('#sysin-register-user-form').find('#sysin-name').val(clientName);
                    $('#sysin-register-user-form').find('#sysin-surname').val(clientSurname);
                    $('#sysin-register-user-form').find('#sysin-email').val(clientEmail);
                    $('#sysin-register-user-form').find('#sysin-cpf').val(clientCPF);
                    $('#sysin-register-user-form').find('#sysin-gender [value="'+ clientGender +'"]').parent().addClass('active');
                    $('#sysin-register-user-form').find('#sysin-birthday').val(clientBirthday);
                    $('#sysin-register-user-form').find('#sysin-permissions').val(clientRoles);

                    if( !clientGender.length ){
                        $('#sysin-register-user-form #sysin-gender').find('.active').removeClass('active');
                    }
                } else {
                    $('#users-modal').find('.modal-title').text('Cadastro de usuário');
                    $('#sysin-register-user-form').find('#sysin-submit').val('Cadastrar usuário');
                    $('#sysin-register-user-form').find('#sysin-user-id').val('');
                    $('#sysin-register-user-form').trigger('reset');
                    $('#sysin-register-user-form #sysin-gender').find('.active').removeClass('active');
                }
            });
        },

        deleteUser: function () {
            $('#delete-user-modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    clientID = button.data('client-id'),
                    clientName = button.data('client-login');

                $('#sysin-delete-user-form').find('#sysin-user-id').val(clientID);
                $('#sysin-delete-user-form').find('.client-data').html(clientName);
            });
        },

        openModalBasedOnURL: function () {

            function getParameterByName(name, url) {
                if (!url) url = window.location.href;
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            }

            var modal = getParameterByName('modal');
            if( modal ){
                $('#' + modal).modal('show');
            }
        }
    };
})(jQuery);