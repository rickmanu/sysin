<?php
/**
 * SysIn functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SysIn
 */

if ( ! function_exists( 'sysin_setup' ) ) :
	function sysin_setup() {

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'sysin' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_image_size('client-thumb', 50, 50, true);

	}
endif;
add_action( 'after_setup_theme', 'sysin_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sysin_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'sysin' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'sysin' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'sysin_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sysin_scripts() {
	wp_enqueue_style( 'sysin-style', get_template_directory_uri() . '/assets/dist/css/bundle.min.css' );

	wp_enqueue_script( 'sysin-scripts', get_template_directory_uri() . '/assets/dist/js/bundle.min.js', array('jquery'), false, true );

	$sysin = array(
		'ajaxUrl' => admin_url( 'admin-ajax.php' ),
		'templatePath' => get_bloginfo('template_url'),
		'templateDir' => get_template_directory(),
	);
	wp_localize_script( 'sysin-scripts', 'sysin', $sysin );
}
add_action( 'wp_enqueue_scripts', 'sysin_scripts' );

/**
 * Includes - Shortcodes
 */
require get_template_directory() . '/inc/crud-client.php';

/* Disable WordPress Admin Bar for all users but admins. */
show_admin_bar(false);

function redirect_non_logged_users() {
	if( !is_user_logged_in() ){
		wp_redirect(  home_url() . '/wp-login.php' );
		exit;
	}
}
add_action( 'template_redirect', 'redirect_non_logged_users' , 0);

function custom_login_redirect( $redirect_to, $request, $user ) {
	return home_url();
	/* if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		if ( in_array( 'administrator', $user->roles ) ) {
			return admin_url();
		} else {
			return home_url();
		}

	}*/
}
// add_filter( 'login_redirect', 'custom_login_redirect', 10, 3 );

function block_users_to_access_admin() {
	if ( is_admin() && !current_user_can( 'manage_options' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
		wp_redirect( home_url() );
		exit;
	}
}
add_action( 'init', 'block_users_to_access_admin' );

function custom_login_title() {
	return 'Login';
}
add_filter( 'login_headertitle', 'custom_login_title' );

function custom_login_footer() { ?>
    <div id="login-footer">
        <h2><i class="fa fa-users"></i> Calaf Check-in</h2>
        <p>©2018 Sistema de controle de entrada de clientes</p>
    </div>
<?php }
add_filter( 'login_footer', 'custom_login_footer' );

function custom_login_page_style() { ?>
	<style type="text/css">
		.login{
            background: #f7f7f7;
		}
		#login{

        }
		#login h1 {
            position: relative;
            text-align: center;
        }
        #login h1:after, #login h1:before {
            content: "";
            height: 1px;
            position: absolute;
            top: 10px;
            width: 20%;
        }
        #login h1:before {
            background: #7e7e7e;
            background: linear-gradient(right,#7e7e7e 0,#fff 100%);
            left: 0;
        }
        #login h1:after {
            background: #7e7e7e;
            background: linear-gradient(right,#7e7e7e 0,#fff 100%);
            right: 0;
        }
		#login h1 a{
            background: none;
            text-indent: inherit;
            line-height: normal;
            height: auto;
            background: #f7f7f7;
            font: 400 25px Helvetica,Arial,sans-serif;
            letter-spacing: -.05em;
            margin: 10px auto 30px;
            color: #73879C;
		}
		.login form{
			background: none !important;
			box-shadow: none !important;
			margin: 0 !important;
			padding: 0 !important;
		}

		#nav a,
		.login label{
			color: #73879C !important;;
		}
		#backtoblog {
			display: none;
		}
		#nav{
			text-align: center;
		}
        #login-footer {
            padding: 20px 0;
            margin: 15px auto;
            width: 320px;
            text-align: center;
            color: #73879C;
            border-top: 1px solid #D8D8D8;
        }
        #login-footer h2{
            font: 400 25px Helvetica,Arial,sans-serif;
            letter-spacing: -.05em;
            line-height: 20px;
            margin: 10px 0 30px;
            text-transform: uppercase;
        }
        #login-footer p {

        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'custom_login_page_style' );

function convert_date_to_sql_format($date_str, $time = false) {
	if( $time ) {
		$date_str = explode( ' ', $date_str );
		$date = $date_str[0];
		$time = $date_str[1];

		$date = explode( '/', $date );
		$date['day'] = $date[0];
		$date['month'] = $date[1];
		$date['year'] = $date[2];

		return $date['year'] . '-' . $date['month'] . '-' .$date['day'] . ' ' . $time . ':00';

    } else {
		$date_str = explode( '/', $date_str );
		$date['day'] = $date_str[0];
		$date['month'] = $date_str[1];
		$date['year'] = $date_str[2];

		return $date['year'] . '-' . $date['month'] . '-' .$date['day'];
    }
}

function client_photo($photo, $register_user) {
    if( !empty($photo) ){
        if (!function_exists('wp_handle_upload')) {
            require_once(ABSPATH . 'wp-admin/includes/file.php');
        }

        $upload_overrides = array('test_form' => false);
        $movefile = wp_handle_upload($photo, $upload_overrides);

        if ($movefile && !isset($movefile['error'])) {
            // Prepare an array of post data for the attachment.
            $attachment = array(
                'guid'           => $movefile['url'],
                'post_mime_type' => $movefile['type'],
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $movefile['file'] ) ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment( $attachment, $movefile['file'] );

            // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
            require_once( ABSPATH . 'wp-admin/includes/image.php' );

            // Generate the metadata for the attachment, and update the database record.
            $attach_data = wp_generate_attachment_metadata( $attach_id, $movefile['file'] );
            wp_update_attachment_metadata( $attach_id, $attach_data );

            add_user_meta( $register_user, '_user_photo', $attach_id, false );

            // echo "File is valid, and was successfully uploaded.\n";
            // var_dump( $movefile );
        } else {
            // No image was uploaded
            /* echo $movefile['error'];
            var_dump( $movefile ); */
        }
    }
}